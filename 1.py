import numpy as np


def str_to_arr(str_vals):
    str_vals = str_vals.replace(' ', '')
    str_vals = str_vals.replace(',', '.')
    exploded = str_vals.split(';')
    return np.array([float(x) for x in exploded])


confidence = 0.95
str_values = """8, 4; 9, 6; 11, 2; 7, 8; 5, 1; 7, 4; 9, 5; 10, 2; 8, 1; 7, 5; 6, 0; 9, 6; 11, 2; 7, 8; 5, 1; 7, 4;
9, 5; 10, 2; 8, 1; 7, 5; 6, 8; 8, 4; 9, 6; 11, 2; 7, 8; 5, 1; 7, 4; 9, 5; 10, 2; 8, 1; 7, 5; 6, 8; 8, 4; 9, 6; 11, 2; 7, 8;
5, 1; 7, 4; 9, 5; 10, 2; 8, 1; 7, 5; 6, 8; 8, 4; 9, 6; 11, 2; 7, 8; 5, 1; 7, 4; 9, 5; 8, 4; 10, 2; 8, 1; 7, 5; 6, 8; 8, 4;
9, 6; 11, 2; 7, 8; 5, 1; 7, 4; 9, 5; 10, 2; 8, 1; 7, 5; 6, 8"""

arr = str_to_arr(str_values)
a = 3.0 * np.array(arr)
n = len(a)
print("Degrees of freedom: " + str(n-1))

mean = np.mean(a)
print("Mean: " + str(mean))
var_est = sum([(xi - mean) ** 2 for xi in a]) / (n - 1)
print("Estimated population variance: " + str(var_est))

se = np.sqrt(var_est) / np.sqrt(n)
print("Sqrt n: " + str(np.sqrt(n)))
print("Std div: " + str(np.sqrt(var_est)))
print("Standard error: " + str(se))
t_value = 2

error = t_value * se

print("{0}% confidence interval: <{1};{2}>".format(confidence * 100, mean - error, mean + error))
