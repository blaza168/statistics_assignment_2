import matplotlib.pyplot as plt

vals = {
    1: 23,
    2: 32,
    3: 36,
    4: 47,
    5: 47,
    6: 50.2,
    7: 55,
    8: 63,
    9: 65,
    10: 70,
    11: 80
}

n = len(vals)
x_sum = sum(vals.keys())
y_sum = sum(vals.values())
xy_sum = sum([x * y for x, y in vals.items()])
x_squared_sum = sum([x**2 for x in vals.keys()])

a = (n* xy_sum - x_sum * y_sum) / (n * x_squared_sum - x_sum**2)
b = (y_sum - a*x_sum) / n

print("Regression line: {0}x + {1}".format(a, b))


y_predicted = [a * x + b for x in vals.keys()]
plt.scatter(vals.keys(), vals.values(), label="Data")
plt.plot(vals.keys(), y_predicted, label="{0:2f}x + {1:2f}".format(a, b))
plt.ylabel("Počet bodů")
plt.xlabel("Číslo předmětu")
plt.grid()
plt.legend()
plt.savefig("img/4.png")
plt.show()
