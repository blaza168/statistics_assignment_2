import math

k = 20
vals = {
    0: 5*k,
    1: 19*k,
    2: 24*k,
    3: 19*k,
    4: 11*k,
    5: 7*k,
    6: 1*k,
    7: 1*k
}

ni_sum = sum(vals.values())

for i, j in vals.items():
    print("{0}: {1} - {2}".format(i, j, j / ni_sum))
print()

n = len(vals)
ex = sum([xi * ni / ni_sum for xi, ni in vals.items()])
ex_squared = sum([xi ** 2 * ni / ni_sum for xi, ni in vals.items()])

print("n: " + str(n))
print("n sum: " + str(ni_sum))
print("E(X) = " + str(ex))
print("(E(X))^2 = " + str(ex ** 2))
print("E(X^2) = " + str(ex_squared))
print("Variance: " + str(ex_squared - ex ** 2))
print("Std: " + str(math.sqrt(ex_squared - ex ** 2)))
