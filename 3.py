# Solve with univariate ANOVA

import pandas as pd
import numpy as np


def calculate_within_group_variation(df):
    total = 0
    for i in range(3):
        group_mean = df.iloc[i].mean()
        for j in range(1, df.shape[1] + 1):
            total += (df.iloc[i][j] - group_mean) ** 2
    return total


def calculate_between_group_variation(df):
    total = 0
    mean = np.average(df)
    print("Mean of all groups combined: " + str(mean))
    for row in df.values:
        total += (np.mean(row) - mean) ** 2
    return total


groups = [
    [4,8,16,15,8,7,8,3,2],
    [7,10,19,18,8,8,8,5,4],
    [7,11,20,18,11,10,11,6,5]
]

df = pd.DataFrame(groups, columns=[i for i in range(1, 10)])

wgv = calculate_within_group_variation(df)
bgv = calculate_between_group_variation(df)

print("Within group variation: " + str(wgv))
print("Between group variation: " + str(bgv))

df1 = df.shape[0] - 1
df2 = df.shape[0] * df.shape[1] - df.shape[0]

print("n: " + str(df.shape[0] * df.shape[1]))

print("Df1: " + str(df1))
print("Df2: " + str(df2))

f = (bgv / df1) / (wgv / df2)

print("F: " + str(f))
